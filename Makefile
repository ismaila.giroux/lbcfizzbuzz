
.PHONY: build build-alpine clean test help default



CMD_NAME ?= server

VERSION := $(shell grep "const Version " pkg/version/version.go | sed -E 's/.*"(.+)"$$/\1/')
GIT_COMMIT=$(shell git rev-parse HEAD)
GIT_DIRTY=$(shell test -n "`git status --porcelain`" && echo "+CHANGES" || true)
BUILD_DATE=$(shell date '+%Y-%m-%d-%H:%M:%S')

default: test

help:
	@echo 'Management commands for lbcfizzbuzz:'
	@echo
	@echo 'Usage:'
	@echo '    make build           Compile the project.'
	@echo '    make get-deps        runs dep ensure, mostly used for ci.'
	@echo '    make build-alpine    Compile optimized for alpine linux.'
	@echo '    make package         Build final docker image with just the go binary inside'
	@echo '    make tag             Tag image created by package with latest, git commit and version'
	@echo '    make test            Run tests on a compiled project.'
	@echo '    make push            Push tagged images to registry'
	@echo '    make clean           Clean the directory tree.'
	@echo

build-cli:
	make -e CMD_NAME=cli build-cmd

build-server:
	make -e CMD_NAME=server build-cmd


build-cmd:
	@echo "building ${CMD_NAME} ${VERSION}"
	@echo "GOPATH=${GOPATH}"
	go build -ldflags "-X github.com/igiroux/lbcfizzbuzz/pkg/version.GitCommit=${GIT_COMMIT}${GIT_DIRTY} -X github.com/igiroux/lbcfizzbuzz/pkg/version.BuildDate=${BUILD_DATE}" \
		-o bin/lbcfizzbuzz-${CMD_NAME} ./cmd/${CMD_NAME}

build: build-cli build-server

get-deps:
	dep ensure

clean:
	rm -fr bin/*

test:
	go test ./...

