# lbcfizzbuzz

LBC Fizz Buzz Test by IGI.

## Getting started

This project requires Go to be installed. On OS X with Homebrew you can just run `brew install go`.

Running it then should be as simple as:

```console
$ make build
$ ./bin/lbcfizzbuzz-server
```

### Testing

``make test``