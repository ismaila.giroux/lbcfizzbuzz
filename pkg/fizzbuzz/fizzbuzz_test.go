package fizzbuzz

import "testing"
import "github.com/stretchr/testify/assert"

func Test(t *testing.T) {
	builder := FizzBuzzBuilder{
		FizzInt: 3,
		BuzzInt: 5,
		FizzStr: "fizz",
		BuzzStr: "buzz",
	}
	assert.Equal(t, "1,2,fizz,4", builder.FizzBuzz(4))
	assert.Equal(t, "1,2,fizz,4,buzz,fizz,7", builder.FizzBuzz(7))
	assert.Equal(t, "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz", builder.FizzBuzz(15))
}
