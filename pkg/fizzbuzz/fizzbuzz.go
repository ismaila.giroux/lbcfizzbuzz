package fizzbuzz

import (
	"strconv"
	"strings"
)

type FizzBuzzBuilder struct {
	FizzInt, BuzzInt int
	FizzStr, BuzzStr string
}

func (builder FizzBuzzBuilder) fizzOrBuzz(intputValue int) string {
	if intputValue % (builder.FizzInt * builder.BuzzInt) == 0 {
		return builder.FizzStr + builder.BuzzStr
	}
	if intputValue % builder.FizzInt == 0 {
		return builder.FizzStr
	}
	if intputValue % builder.BuzzInt == 0 {
		return builder.BuzzStr
	}
	return strconv.Itoa(intputValue)
}

func (builder FizzBuzzBuilder) FizzBuzz(limit int) string {

	var fizzBuzzList []string
	for i := 1; i <= limit; i++ {
		fizzBuzzList = append(fizzBuzzList, builder.fizzOrBuzz(i))
	}
	return strings.Join(fizzBuzzList, ",")
}
