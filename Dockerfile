# Build Stage
FROM lacion/alpine-golang-buildimage:1.13 AS build-stage

LABEL app="build-lbcfizzbuzz"
LABEL REPO="https://github.com/igiroux/lbcfizzbuzz"

ENV PROJPATH=/go/src/github.com/igiroux/lbcfizzbuzz

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:$GOROOT/bin:$GOPATH/bin

ADD . /go/src/github.com/igiroux/lbcfizzbuzz
WORKDIR /go/src/github.com/igiroux/lbcfizzbuzz

RUN make build-alpine

# Final Stage
FROM lacion/alpine-base-image:latest

ARG GIT_COMMIT
ARG VERSION
LABEL REPO="https://github.com/igiroux/lbcfizzbuzz"
LABEL GIT_COMMIT=$GIT_COMMIT
LABEL VERSION=$VERSION

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:/opt/lbcfizzbuzz/bin

WORKDIR /opt/lbcfizzbuzz/bin

COPY --from=build-stage /go/src/github.com/igiroux/lbcfizzbuzz/bin/lbcfizzbuzz /opt/lbcfizzbuzz/bin/
RUN chmod +x /opt/lbcfizzbuzz/bin/lbcfizzbuzz

# Create appuser
RUN adduser -D -g '' lbcfizzbuzz
USER lbcfizzbuzz

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["/opt/lbcfizzbuzz/bin/lbcfizzbuzz"]
