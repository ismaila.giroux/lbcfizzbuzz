module github.com/igiroux/lbcfizzbuzz

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi/v5 v5.0.0
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	gotest.tools v2.2.0+incompatible
)
