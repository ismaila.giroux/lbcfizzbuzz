package main

import (
	"flag"
	"fmt"
	"strconv"

	"github.com/igiroux/lbcfizzbuzz/pkg/fizzbuzz"
	"github.com/igiroux/lbcfizzbuzz/pkg/version"
)

func main() {

	versionFlag := flag.Bool("version", false, "Version")
	flag.Parse()

	if *versionFlag {
		fmt.Println("Build Date:", version.BuildDate)
		fmt.Println("Git Commit:", version.GitCommit)
		fmt.Println("Version:", version.Version)
		fmt.Println("Go Version:", version.GoVersion)
		fmt.Println("OS / Arch:", version.OsArch)
		return
	}
	if len(flag.Args()) != 1 {
		return
	}
	inputValue, err := strconv.Atoi(flag.Args()[0])
	if err != nil {
		panic(err)
	}
	builder := fizzbuzz.FizzBuzzBuilder{
		FizzInt: 3,
		BuzzInt: 5,
		FizzStr: "fizz",
		BuzzStr: "buzz",
	}
	fmt.Println(builder.FizzBuzz(inputValue))

}
