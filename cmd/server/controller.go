package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/igiroux/lbcfizzbuzz/pkg/fizzbuzz"
)

// FizzBuzzReq is request deserializer.
type FizzBuzzReq struct {
	Int1  int    `json:"int1"`
	Int2  int    `json:"int2"`
	Limit int    `json:"limit"`
	Str1  string `json:"str1"`
	Str2  string `json:"str2"`
}

// ParseRequest returns parsed payload
func ParseRequest(r *http.Request) (*FizzBuzzReq, error) {
	payload := FizzBuzzReq{}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errors.New("unable to read body")
	}

	err = json.Unmarshal(body, &payload)
	if err != nil {
		return nil, errors.New("unable to parse body")
	}
	if payload.Int1 <= 1 || payload.Int2 <= 1 {
		return nil, errors.New("zero value for field 'int1' or 'int2'")
	}
	if strings.Trim(payload.Str1, " ") == "" || strings.Trim(payload.Str1, " ") == "" {
		return nil, errors.New("empty value for field 'str1' or 'str2'")
	}

	return &payload, nil
}

// Handle 400
func badRequest(w http.ResponseWriter, err error) {
	msg := fmt.Sprintf("bad gateway: %s", err.Error())
	fmt.Println(http.StatusBadRequest, msg)
	w.WriteHeader(http.StatusBadRequest)
	_, err = w.Write([]byte("400 bad gateway"))
	if err != nil {
		fmt.Println("error writing the response body", err.Error())
	}
}

// FizzBuzzHandler handles HTTP requests
func FizzBuzzHandler(w http.ResponseWriter, r *http.Request) {
	payload, err := ParseRequest(r)
	if err != nil {
		badRequest(w, err)
		return
	}
	builder := fizzbuzz.FizzBuzzBuilder{
		FizzInt: payload.Int1,
		BuzzInt: payload.Int2,
		FizzStr: payload.Str1,
		BuzzStr: payload.Str2,
	}
	w.Write([]byte(builder.FizzBuzz(payload.Limit)))
}
