package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(FizzBuzzHandler))
	defer server.Close()

	type TSuite struct {
		payload        string
		expectedStatus int
	}
	suites := []TSuite{
		TSuite{payload: "{}", expectedStatus: 400},
		TSuite{payload: `{"int1": 3, "limit": 10, "str1": "fizz", "str2": "buzz"}`, expectedStatus: 400},
		TSuite{payload: `{"int1": 3, "int2": 5, "limit": 10, "str1": "fizz", "str2": "buzz"}`, expectedStatus: 200},
	}
	for _, tst := range suites {

		req, err := http.NewRequest("POST", fmt.Sprintf("http://%s/", server.Listener.Addr()), strings.NewReader(tst.payload))
		if err != nil {
			panic(err)
		}
		rsp, err := http.DefaultClient.Do(req)

		if err != nil {
			t.Fatalf("http.DefaultClient.Do failed: %s", err)
		}
		assert.Equal(t, tst.expectedStatus, rsp.StatusCode)
	}

}
